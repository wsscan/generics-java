import java.util.ArrayList;
import java.util.List;

public class GameSingle implements InterfaceGame<EnemySingle> {

    public List<EnemySingle> enemiesList;
    public Pool<EnemySingle> enemiesPool;

    GameSingle() {
        enemiesList = new ArrayList<>();
        enemiesPool = new Pool<EnemySingle>() {
            @Override protected EnemySingle newInstance() {
                return new EnemySingle();
            }
            @Override void freeObject(EnemySingle object) {
                super.freeObject(object);
                enemiesList.remove(object);
            }
        };

        EnemySingle enemy = enemiesPool.obtain();
        // Установили врагу снаряд, а снаряду врага
        ShellEnemy<EnemySingle> shell = new ShellEnemy<>();
        shell.setOwner(enemy);
        enemy.setShell(shell);
        enemiesList.add(enemy);
    }

    public List<EnemySingle> getEnemiesList() {
        return enemiesList;
    }
}
