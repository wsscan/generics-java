import java.util.ArrayList;
import java.util.List;

public class GameMulti implements InterfaceGame<EnemyMulti> {

    public List<EnemyMulti> enemiesList;
    public Pool<EnemyMulti> enemiesPool;

    GameMulti() {
        enemiesList = new ArrayList<>();
        enemiesPool = new Pool<EnemyMulti>() {
            @Override protected EnemyMulti newInstance() {
                return new EnemyMulti();
            }
            @Override void freeObject(EnemyMulti object) {
                super.freeObject(object);
                enemiesList.remove(object);
            }
        };

        EnemyMulti enemy = enemiesPool.obtain();
        // Установили врагу снаряд, а снаряду врага
        ShellEnemy<EnemyMulti> shell = new ShellEnemy<>();
        shell.setOwner(enemy);
        enemy.setShell(shell);
        enemiesList.add(enemy);
    }

    public List<EnemyMulti> getEnemiesList() {
        return enemiesList;
    }
}
