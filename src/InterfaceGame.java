import java.util.List;

public interface InterfaceGame<T extends InterfaceEnemy> {
    List<T> getEnemiesList();
}
