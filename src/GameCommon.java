import java.util.ArrayList;
import java.util.List;

class GameCommon {

    public List<Enemy> enemiesList;
    public Pool<Enemy> enemiesPool;

    GameCommon() {
        enemiesList = new ArrayList<>();
        enemiesPool = new Pool<Enemy>() {
            @Override protected Enemy newInstance() {
                return new Enemy();
            }
            @Override void freeObject(Enemy object) {
                super.freeObject(object);
                enemiesList.remove(object);
            }
        };

        Enemy enemy = enemiesPool.obtain();
        // Установили врагу снаряд, а снаряду врага
        Shell shell = new Shell();
        shell.setOwner(enemy);
        enemy.setShell(shell);
        enemiesList.add(enemy);
    }
}
