import java.util.ArrayList;

abstract class Pool<T> {

    private final ArrayList<T> freeObjects;

    Pool() {
        freeObjects = new ArrayList<>();
    }

    protected abstract T newInstance();

    T obtain() {
        return freeObjects.size() > 0 ? freeObjects.get(0) : this.newInstance();
    }

    int size() {
        return freeObjects.size();
    }

    void freeObject(T object) {
        freeObjects.add(object);
    }
}
