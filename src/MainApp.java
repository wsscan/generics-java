
public class MainApp{
    public static void main(String[] args) {
        GameCommon common = new GameCommon();
        for (Enemy enemy: common.enemiesList) {
            enemy.getShell().printId();
        }
        GameMulti multi = new GameMulti();
        for (EnemyMulti enemy: multi.enemiesList) {
            enemy.getShell().printId();
        }
        GameSingle single = new GameSingle();
        for (EnemySingle enemy: single.enemiesList) {
            enemy.getShell().printId();
        }
    }
}
/** GameCommon */
// Класс врага для GameCommon
class Enemy {
    private Shell shell;
    Shell getShell() {
        return this.shell;
    }
    void setShell(Shell shell) {
        this.shell = shell;
    }
    void printSelf() {
        System.out.println("I am Enemy");
   }
}
// Снаряд для Enemy
class Shell {
    private Enemy owner;
    void setOwner(Enemy owner) {
        this.owner = owner;
    }
    void printId() {
        owner.printSelf();
    }
}

/** GameSingle, GameMulti */
// Интерфейс врага для EnemySingle и EnemyMulti и ShellEnemy
interface InterfaceEnemy{
    ShellEnemy getShell();
    void printSelf();
}

// Снаряд для Enemy
class ShellEnemy<T extends InterfaceEnemy> {
    private T owner;
    void setOwner(T owner) {
        this.owner = owner;
    }
    T getOwner() {
        return this.owner;
    }
    void printId() {
        owner.printSelf();
    }
}
// Класс врага для GameSingle, который наследует общий интерфейс врага InterfaceEnemy
class EnemySingle implements InterfaceEnemy {
    private ShellEnemy shell;
    public ShellEnemy getShell() { return this.shell; }
    void setShell(ShellEnemy shell) { this.shell = shell; }
    @Override public void printSelf() {
        System.out.println("I am EnemySingle");
    }
}
// Класс врага для GameMulti, который наследует общий интерфейс врага InterfaceEnemy
class EnemyMulti implements InterfaceEnemy {
    private ShellEnemy shell;
    public ShellEnemy getShell() { return this.shell; }
    void setShell(ShellEnemy shell) { this.shell = shell; }
    @Override public void printSelf() {
        System.out.println("I am EnemyMulti");
    }
}